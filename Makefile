include Makefile.conf

all:
	make -sC src
	make -sC lua
	make -sC tools 

install:
	make -sC src install
	make -sC lua install
	make -sC tools install
	mkdir -p /etc/ubx/
	@echo 'export    UBX_INSTALL=$(INSTALL_PREFIX);' > /etc/ubx/ubx-core
	@echo 'export    UBX_MODULES=$(INSTALL_PREFIX)/lib/ubx/;' >> /etc/ubx/ubx-core
	@echo 'export     UBX_MODELS=$(INSTALL_PREFIX)/share/ubx/models/;' >> /etc/ubx/ubx-core
	@echo 'export UBX_METAMODELS=$(INSTALL_PREFIX)/share/ubx/metamodels;' >> /etc/ubx/ubx-core
	@echo 'export   UBX_LUA_PATH=$(INSTALL_PREFIX)/share/ubx/lua/?.lua;' >> /etc/ubx/ubx-core
	@echo >> /etc/ubx/ubx-core
	@echo 'if [[ "x$$LUA_PATH" == "x" ]]; then export LUA_PATH=";;"; fi' >> /etc/ubx/ubx-core
	@echo 'export LUA_PATH="$$UBX_LUA_PATH;$$LUA_PATH"' >> /etc/ubx/ubx-core
	@echo >> /etc/ubx/ubx-core

clean:
	make -sC src clean
	make -sC lua clean
	make -sC tools clean 

.PHONY: all install clean

